# HTML Renderer

A micro-service for rendering HTML to PDFs or images.

## Endpoints

### Render HTML

`/render`

#### Request

```json
{
    "html": <string>,
    "render_response": <string>,
    "config": {
        "quality": <int>,
        "width": <string>,
        "height": <string>,
        "format": <string>,
        "orientation": <string>,
        "type": <string>,
        "border": {
            "top": <string>,
            "right": <string>,
            "bottom": <string>,
            "left": <string>,
        }
    }
}
```

Options

Value | Description | Default
--- | --- | ---
html | HTML string to be rendered | [required]
render_response | Render response _(allowed values: buffer, base64)_ | `buffer`
quality | Image quality | `75`
width | Render width _(allowed units: mm, cm, in, px)_ | `8in`
height | Render height _(allowed units: mm, cm, in, px)_ | `10.5in`
format | Page format _(allowed units: A3, A4, A5, Legal, Letter, Tabloid)_ | `A4`
orientation | Page orientation _(Portrait or landscape)_ | `Portrait`
border.top | Top margin _(allowed units: mm, cm, in, px)_ | `0`
border.right | Right margin _(allowed units: mm, cm, in, px)_ | `0`
border.bottom | Bottom margin _(allowed units: mm, cm, in, px)_ | `0`
border.left | Left margin _(allowed units: mm, cm, in, px)_ | `0`
type | Render type _(allowed file types: png, jpeg, pdf)_ | `pdf`

> NOTE: When the page width and page height are specified, the size and orientation are ignored.

#### Response

Buffer Response:

```json
{
    "status": <string>,
    "data": {
        "type": "Buffer",
        "data": <byte-array>
    }
}
```

Base64 Response:

```json
{
    "status": <string>,
    "data": <string>
}
```
