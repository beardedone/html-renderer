FROM node:8-slim

LABEL authors="Michael K. Essandoh <mexcon.mike@gmail.com>"

ENV NODE_ENV=Production
ENV NODE_PORT=80

# Install PhantomJS & dependencies
RUN apt-get update && \
    apt-get install -y python curl build-essential chrpath libssl-dev libxft-dev libfreetype6 libfreetype6-dev libfontconfig1 libfontconfig1-dev && \
    wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 && \
    tar xvjf phantomjs-2.1.1-linux-x86_64.tar.bz2 -C /usr/local/share/ && \
    ln -sf /usr/local/share/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin && \
    rm phantomjs-2.1.1-linux-x86_64.tar.bz2 && \
    apt-get clean
    # apt autoclean && apt autoremove 

RUN mkdir -p /usr/share/fonts/truetype/calibri
ADD fonts /usr/share/fonts/truetype/calibri/

RUN mkdir -p /app && chmod -R 777 /app
ADD . /app
WORKDIR /app
RUN npm install

EXPOSE $NODE_PORT

CMD ["npm", "start"]