const _env = process.env.NODE_ENV || "Development";
const _port = process.env.NODE_PORT || 2000;

const express = require("express"),
      logger = require("morgan"),
      { checkSchema, validationResult } = require("express-validator"),
      compression = require("compression"),
      pdf = require("html-pdf");

const app = express();

app.use(logger("combined"));
app.use(express.json({limit: '500mb'}));
app.use(express.urlencoded({limit: '500mb'}));

app.use(compression());

app.use(function(err, req, res, next) {
  console.log("Error :: " + err);
  res.status( err.code || 500 )
      .json({
        status: "error",
        message: err.message
      });
});

// routes
app.get("/", function(req, res) {
  res.json({ message: "HTML Renderer" });
});

app.post('/render', checkSchema({
    html: {
        notEmpty: true,
        errorMessage: 'HTML body is required'
    }
  }), (req, res, next) => {
    const errors = validationResult(req);
    
    if (!errors.isEmpty()) {
        return res.status(400)
                  .json({
                    status: 'error',
                    errors: errors.array()
                  });
    }

    pdf.create(req.body.html, req.body.config).toBuffer(function(err, buffer){
        if (err) {
            console.log('Render Error :: ' + err);
            return res.status(500)
                      .json({
                          status: 'error',
                          message: err.message
                      });
        }

        const renderResponse = req.body.render_response || 'buffer';

        if (renderResponse === 'base64') {
          return res.json({
            status: 'success',
            data: buffer.toString("base64")
          });
        }

        return res.json({
            status: 'success',
            data: buffer
        });
    });
});

app.listen(_port);
console.log("HTML Renderer :: Started on " + _port + " :: " + _env);
